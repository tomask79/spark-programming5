# Spark programming 5 #

This time during learning to [Hortonworks Spark Certification](https://hortonworks.com/services/training/certification/hdp-certified-spark-developer/)
I came across to the interesting problem. Let's assume following problem:

Big data of **students** (id, name):

	1,John
	2,Marc
	3,Ted
	4,Jerry
	5,Tom
	6,Alfred


Big data of **attendance** (student_id,...**various array of attendance expressed by date**)

	1,01.12.2018,01.15.2018,01.17.2018,01.20.2018
	2,01.12.2018,01.15.2018
	6,01.15.2018,01.17.2018
	4,01.17.2018

Task:

Get the attendance of student who has the biggest attendance after 13.1.2018?

Solution:

1) Parsing the input:

	case class Student(id: Int,
			   name: String);

	val students = sc.textFile("/tests/students.txt");
	val attendence = sc.textFile("/tests/attendence.txt");

	val studentsObjects = students.map(line=>(line.split(",")(0).toInt, Student(line.split(",")(0).toInt,line.split(",")(1))));

2) Breaking the attendance into the key-value pairs (student_id, date):

	val attendencePairs = attendence.flatMap(line=>{
		for(i <- 1 to line.split(",").length-1) yield {
			(line.split(",")(0).toInt, line.split(",")(i));
		}
	}
	);

**The idea is**: Scala comes with excellent solution for emitting multiple key-value pairs in the for loop.
It's called **yielding**. Where [yield](https://alvinalexander.com/scala/scala-for-loop-yield-examples-yield-tutorial) 
is some kind of temporary buffer which we'll make flat by calling flatMap in the end.

3) Getting the student with biggest attendance **after 13.1.2018**:

	val filterDate = LocalDate.of(2018,1,13);

	val studentPairs = attendencePairs.filter(at=>{
		         val localDate = LocalDate.parse(at._2, DateTimeFormatter.ofPattern("MM.dd.yyyy"));
			 localDate.isAfter(filterDate);
	});

	val countPairs = studentPairs.map(st=>(st._1,1)).reduceByKey((a,b)=>a+b).map(st=>(st._2,st._1)).sortByKey(false).map(st=>(st._2, st._1));

	val studentResult = countPairs.take(1)(0);

Nothing special in here, I just used **java.time.LocalDate** and **java.time.format.DateTimeFormat** classes
for filtering the rows according the date.

4) Printing the attendace of the most attending student after 13.1.2018.

	val joinResult = studentPairs.join(studentsObjects.filter(st=>st._2.id.equals(studentResult._1)));

	val printResult = joinResult.map(at=>(at._2._2.name, at._2._1));

	printResult.collect();

** Getting the result **:

- put both attached files to your hadoop hortonworks sandbox installation.
- spark-shell -i spark8.scala

In the end you should see:

joinResult: org.apache.spark.rdd.RDD[(Int, (String, Student))] = MapPartitionsRDD[17] at join at <console>:47
printResult: org.apache.spark.rdd.RDD[(String, String)] = MapPartitionsRDD[18] at map at <console>:49
res0: Array[(String, String)] = Array((John,01.15.2018), (John,01.17.2018), (John,01.20.2018))

best regards

Tomas